#RideBeam Assignment

Require install the latest version of [docker-machine](https://docs.docker.com/docker-for-mac/install/) and [docker-machine](https://docs.docker.com/compose/install/)

### Project structure
```
.
├── part-01                    # The solution for problem 1
│   ├── api                    # Source for Backend
│   ├── db                     # Source and config for postgres database
│   ├── web                    # Source for Frontend
├── part-02     
│   ├── ridebeam-part02.pdf    # The solution for problem 2
├── docker-compose.yml         # the docker-compose file to manage multiple docker
├── README.md
└── ...
```

## Test problem 01

Run the command to build the docker containers

```bash
docker-compose build
```

run all docker containers

**NOTE: ** will have an error about can't connect to the database for the first time, because the database needs the time to init. Please run `docker-compose down` and up again to solve this error
```bash
docker-compose up -d
```

## Shutdown docker

```bash
docker-compose down
```
