package scooter

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"bitbucket.org/trinhquocviet/ridebeam-assignment/part-01/api/common/helpers"
)

type Service struct {}


func (s *Service) Get(req *GetReq) (scooterList []*GetRes, err error) {
	db, err := gorm.Open("postgres", helpers.ConnectionStr())
	defer db.Close()

	if err == nil {
		var tempScooterList []*Scooter
		tableName := fmt.Sprintf("get_scooters(%d, %d, '%f,%f')", req.radius, req.numberOfScooters, req.lat, req.lng);
		if err := db.Table(tableName).Scan(&tempScooterList).Error; err != nil {
			return nil, err
		}

		for _, s := range tempScooterList {
			var scooter GetRes
			scooter.Mapping(s)
			scooterList = append(scooterList, &scooter)
		}
	}
	return
}