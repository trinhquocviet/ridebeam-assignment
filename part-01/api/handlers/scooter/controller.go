package scooter

import (
	"strconv"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"net/http"
	"github.com/labstack/echo"
	"github.com/Sirupsen/logrus"
)

type Controller struct {
	service Service
}

func (c *Controller) Get() echo.HandlerFunc {
	return func(e echo.Context) error {
		var req = new(GetReq)
		req.radius, _ = strconv.Atoi(e.QueryParam("radius"))
		req.numberOfScooters, _ = strconv.Atoi(e.QueryParam("numberOfScooters"))
		req.lat, _ = strconv.ParseFloat(e.QueryParam("lat"), 64)
		req.lng, _ = strconv.ParseFloat(e.QueryParam("lng"), 64)

		err := req.Validate()
		if err != nil {
			return e.JSON(http.StatusBadRequest, err)
		}

		scooterList, err := c.service.Get(req)
		if err != nil {
			logrus.Error(err)
			return e.JSON(http.StatusBadRequest, []GetRes{})
		}

		if (len(scooterList) <= 0) {
			return e.JSON(http.StatusOK, []GetRes{})
		}

		return e.JSON(http.StatusOK, scooterList)
	}
}