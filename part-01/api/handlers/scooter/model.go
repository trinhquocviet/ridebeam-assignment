package scooter

import (
	"time"
	"errors"
	"regexp"
	"strconv"
	"strings"
)

type Scooter struct {
	ID           uint       `gorm:"column:id"`
	DeviceID     string     `gorm:"column:device_id"`
	Location     string     `gorm:"column:location"`
	CreatedAt    time.Time  `gorm:"column:created_at"`
	UpdatedAt    time.Time  `gorm:"column:updated_at"`
	DeletedAt    time.Time  `gorm:"column:deleted_at"`
}

type GetReq struct {
	radius   					int			`query:"radius"`
	numberOfScooters	int			`query:"numberOfScooters"`
	lat  							float64	`query:"lat"`
	lng								float64	`query:"lng"`
}
func (g *GetReq) Validate() (err error) {
	err = nil
	if g.radius < 0 {
		err = errors.New("The radius shouldn't less than 1")
	} else if g.numberOfScooters < 0 {
		err = errors.New("The radius shouldn't less than 1")
	}

	return
}

type GetRes struct {
	ID					uint												`json:"id"`
	DeviceID		string											`json:"device_id"`
	Location		struct {
								Lat float64								`json:"lat"`
								Lng float64								`json:"lng"`
							}														`json:"location"`
}
func (g *GetRes) Mapping(s *Scooter) {
	g.ID = s.ID
	g.DeviceID = s.DeviceID
	
	rgx := regexp.MustCompile("\\(|\\)")
	loc := strings.Split(rgx.ReplaceAllString(s.Location, ""), ",");
	g.Location.Lat, _ = strconv.ParseFloat(loc[0], 64)
	g.Location.Lng, _ = strconv.ParseFloat(loc[1], 64)
}
