package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sandalwing/echo-logrusmiddleware"
	"github.com/spf13/viper"
	"bitbucket.org/trinhquocviet/ridebeam-assignment/part-01/api/routes"
	"strings"
)

func main() {
	err := loadConfiguration()
	if err != nil {
		fmt.Println("An error occurred: ", err)
	} else {
		e := configEchoFramework()
		routes.SetupAPIRoutes(e)

		// Server
		e.Logger.Fatal(e.Start(":1313"))
	}
}

// configEchoFramework will return echo variable already config
func configEchoFramework() *echo.Echo {
	e := echo.New()
	e.Logger = logrusmiddleware.Logger{logrus.StandardLogger()}
	e.Use(logrusmiddleware.Hook())

	// Middleware
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: strings.Split(viper.GetString("HeaderAllowOrigins"), ","),
		AllowMethods: strings.Split(viper.GetString("HeaderAllowMethods"), ","),
		AllowHeaders: strings.Split(viper.GetString("HeaderAllowHeaders"), ","),
	}))

	return e
}

// loadConfiguration will read and get config from config file, return error if something wrong
func loadConfiguration() error {
	viper.SetConfigName("develop.config")
	viper.SetConfigType("json")
	viper.AddConfigPath("./common/config")
	err := viper.ReadInConfig()
	if err != nil {
		return err
	}
	return nil
}
