package routes

import (
	"github.com/labstack/echo"
	"bitbucket.org/trinhquocviet/ridebeam-assignment/part-01/api/handlers/scooter"
)

func SetupAPIRoutes(e *echo.Echo) {
	api := e.Group("/api")
	
	var ScooterController scooter.Controller
	api.GET("/scooter", ScooterController.Get())
}
