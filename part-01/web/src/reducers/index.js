import { combineReducers } from 'redux';
// 
import scooter from './scooter';
import form from './form';

export default combineReducers({
  scooter,
  form,
});
