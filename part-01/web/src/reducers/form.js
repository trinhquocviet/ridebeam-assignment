import { DEFAULT_GEO_LOCATION } from 'common/constants/env';
import TYPES from 'common/constants/actionTypes';

const INITIAL_STATE = {
  radius:           1,
  location:         DEFAULT_GEO_LOCATION,
};

export default (state = INITIAL_STATE, action) => {
  const { SAVE } = TYPES.FORM;
  // 
  switch (action.type) {
    case SAVE:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
