import TYPES from 'common/constants/actionTypes';
import { getReducerData, removeTypeState } from 'common/helpers/reducer';

const INITIAL_STATE = {
  scooters:           [],
  totalScooters:      0,

  isGettingScooters:  false,
  getScootersError:   '',
};

export default (state = INITIAL_STATE, action) => {
  const { GET } = TYPES.SCOOTER;
  const type = removeTypeState(action.type);
  // 
  switch (type) {
    case GET:
      return getReducerData(state, action, GET, {
        loading: 'isGettingScooters',
        error: 'getScootersError',
      });
    default:
      return state;
  }
};
