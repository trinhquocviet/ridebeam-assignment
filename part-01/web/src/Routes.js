import React from 'react';
import {
  Switch,
  Route,
  withRouter,
} from 'react-router-dom';
// import { ROUTES } from 'common/constants/routes';
import {
  Home
} from 'pages';

const Routes = () => (
  <Switch>
    <Route path="*" component={Home} />
  </Switch>
);

export default withRouter(Routes);
