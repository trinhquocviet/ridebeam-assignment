import { values } from 'lodash';
import { flatten } from 'common/helpers/object';

const TYPES = {
  FORM: {
    SAVE: 'SAVE_FORM',
  },
  SCOOTER: {
    GET: 'GET_SCOOTER',
  },
}

export default TYPES;
export const ACTION_KEYS = values(flatten(TYPES));
