const ROUTES = {
  DASHBOARD: {
    key: 'dashboard',
    link: '/dashboard',
    name: 'Dashboard',
  }
};

export {
  ROUTES,
};
