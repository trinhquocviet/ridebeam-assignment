import buildUrl from 'build-url';
// ---
const BASE_URL = buildUrl(process.env.REACT_APP_API);

// this block define all url link
// ? URL is fixed version will no need version
const API_SCOOTER = buildUrl(BASE_URL, { path: 'scooter' });

// ? URL is NOT fixed version

// ? ...


export {
  API_SCOOTER,
};
