import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Navbar,
} from 'reactstrap';
import classNames from 'common/helpers/classnames';
import brandIcon from 'common/assets/images/scooter.svg';
import styles from './Navigation.module.scss';

export default class Navigation extends Component {
  static propTypes = {
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  render = () => {
    const { className } = this.props;

    return (
      <Navbar
        expand="md"
        className={classNames([styles.wrapper, className])}
      >
        <a className={styles.brand} href="/">
          <span className={styles.brandText}>SCOOTER FINDER</span>
          <img src={brandIcon} className={styles.brandIcon} alt="Scooter Finder" />
        </a>
      </Navbar>
    )
  }
}
