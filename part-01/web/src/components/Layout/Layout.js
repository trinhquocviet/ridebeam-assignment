import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'common/helpers/classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import Navigation from 'components/Navigation';
import Filter from 'components/Filter';
import styles from './Layout.module.scss';

export default class Layout extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    containerClass: PropTypes.string,
    contentClass: PropTypes.string,
  };

  static defaultProps = {
    containerClass: '',
    contentClass: '',
  };

  state = {
    containerPaddingTop: 0,
  }

  componentDidMount = () => {
    this.setState({ containerPaddingTop: this.getHeaderHeight() });
    window.addEventListener('resize', this.resizeContent);
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.resizeContent);
  }

  getHeaderHeight = () => {
    const headerEl = document.querySelector(`.${styles.header}`);
    return headerEl.offsetHeight;
  }

  resizeContent = () => {
    this.setState({ containerPaddingTop: this.getHeaderHeight() });
  }

  render = () => {
    const {
      children,
      contentClass,
      containerClass,
    } = this.props;

    const {
      containerPaddingTop
    } = this.state;

    return (
      <Scrollbars
        autoHide
        className={styles.wrapper}
      >
        <div className={styles.header}>
          <Navigation className={styles.navigation} />
          <Filter className={styles.filter} />
        </div>
        <div
          className={classNames([styles.container, containerClass])}
          style={{paddingTop: `${containerPaddingTop}px`}}
        >
          <div
            className={classNames([styles.content, contentClass])}
          >
            {children}
          </div>
        </div>
      </Scrollbars>
    )
  }
}
