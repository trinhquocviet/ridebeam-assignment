import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { MAX_NUMBER_OF_SCOOTER } from 'common/constants/env';
import classNames from 'common/helpers/classnames';
import FormGroup from '../FormGroup';
import styles from './NumberOfScooters.module.scss';

export default class NumberOfScooters extends Component {
  static propTypes = {
    className:        PropTypes.string,
    numberOfScooters: PropTypes.number,
    //
    onChange:         PropTypes.func,
  }

  static defaultTypes = {
    className:        '',
    numberOfScooters: 0,
    // 
    onChange:   () => {},
  }

  handleOnChangeValue = (value) => {
    const { onChange } = this.props;
    let newValue = (typeof(value) === 'string') ? parseInt(value, 10) : value;
    if (newValue > MAX_NUMBER_OF_SCOOTER) {
      newValue = MAX_NUMBER_OF_SCOOTER;
    } else if (newValue < 1) {
      newValue = 1;
    }
    // 
    onChange(newValue);
  }

  render = () => {
    const {
      className,
      numberOfScooters,
    } = this.props;

    return (
      <div className={classNames([styles.wrapper, className])}>
        <FormGroup label="Number Of Scooters">
          <InputGroup size="sm">
            <Input
              type="number"
              name="search-radius"
              id="search-radius"
              min="1"
              max={MAX_NUMBER_OF_SCOOTER}
              value={numberOfScooters}
              placeholder="100"
              onChange={e => this.handleOnChangeValue(e.target.value)}
            />
            <InputGroupAddon addonType="append">
              <InputGroupText>
                Scooters
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
      </div>
    )
  }
}
