import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { isEmpty } from 'lodash';
import { DEFAULT_GEO_LOCATION } from 'common/constants/env';
import classNames from 'common/helpers/classnames';
import FormGroup from '../FormGroup';
import styles from './Location.module.scss';

export default class Location extends Component {
  static propTypes = {
    className:  PropTypes.string,
    location:   PropTypes.object,
    // 
    onChange:   PropTypes.func,
  }
  static defaultProps = {
    className:  '',
    location:   DEFAULT_GEO_LOCATION,
    // 
    onChange:   () => {},
  }

  state = {
    lat: DEFAULT_GEO_LOCATION.lat,
    lng: DEFAULT_GEO_LOCATION.lng,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let newState = {};

    const [newLat, oldLat] = [nextProps.location.lat, prevState.lat];
    if (newLat !== oldLat) {
      newState.lat = newLat;
    }

    const [newLng, oldLng] = [nextProps.location.lng, prevState.lng];
    if (newLng !== oldLng) {
      newState.lng = newLng;
    }

    return (isEmpty(newState)) ? null : newState;
  }

  handleOnChange = (key, value) => {
    const { onChange } = this.props;
    const { lat, lng } = this.state;
    // const transformedValue = (/\.$/g.test(value)) ? parseFloat(`${value}.0`) : parseFloat(value || 0);
    // console.log(transformedValue);
    let geo = {
      lat: (key === 'lat') ? value : lat,
      lng: (key === 'lng') ? value : lng,
    }

    onChange(geo);
  }

  render = () => {
    const { className } = this.props;
    const { lat, lng } = this.state;

    return (
      <div className={classNames([styles.wrapper, className])}>
        <FormGroup label="Location">
          <Row>
            <Col>
              <InputGroup size="sm">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    Latitude
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  type="number"
                  step="0.000001"
                  name="latitude"
                  id="latitude"
                  placeholder="Latitude"
                  className={styles.latitude}
                  value={lat}
                  onChange={e => this.handleOnChange('lat', e.target.value)}
                />
              </InputGroup>
            </Col>

            <Col xs="auto">
              <InputGroup size="sm">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    Longitude
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  type="number"
                  step="0.000001"
                  name="longitude"
                  id="longitude"
                  placeholder="Longitude"
                  className={styles.longitude}
                  value={lng}
                  onChange={e => this.handleOnChange('lng', e.target.value)}
                />
              </InputGroup>
            </Col>
          </Row>
        </FormGroup>
      </div>
    )
  }
}
