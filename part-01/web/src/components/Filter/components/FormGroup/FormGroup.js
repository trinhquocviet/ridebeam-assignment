import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'common/helpers/classnames';
import styles from './FormGroup.module.scss';

export default class FormGroup extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    label: PropTypes.string,
  }

  static defaultProps = {
    className: '',
    children: '',
    label: '',
  }

  render = () => {
    const {
      label,
      children,
      className,
    } = this.props;

    return (
      <div className={classNames([styles.wrapper, className])}>
        <div className={styles.heading}>
          <span className={styles.label}>{ label }</span>
        </div>
        <div className={styles.content}>
          { children }
        </div>
      </div>
    )
  }
}
