import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import { MAX_RADIUS } from 'common/constants/env';
import classNames from 'common/helpers/classnames';
import Slider from 'rc-slider/lib/Slider';
import FormGroup from '../FormGroup';
import styles from './SearchRadius.module.scss';

export default class SearchRadius extends Component {
  static propTypes = {
    className:  PropTypes.string,
    radius:     PropTypes.number,
    // 
    onChange:   PropTypes.func,
  }

  static defaultTypes = {
    className:  '',
    radius:     0,
    // 
    onChange: () => {},
  }

  handleOnChangeValue = (value) => {
    const { onChange } = this.props;
    let newValue = (typeof(value) === 'string') ? parseInt(value || 0, 10) : value;
    if (newValue > MAX_RADIUS) {
      newValue = MAX_RADIUS;
    } else if (newValue < 1) {
      newValue = 1;
    }
    // 
    onChange(newValue);
  }

  render = () => {
    const {
      className,
      radius,
    } = this.props;
    return (
      <div className={classNames([styles.wrapper, className])}>
        <FormGroup label="Search Radius">
          <Row className="align-items-center">
            <Col>
              <Slider
                min={1}
                max={MAX_RADIUS}
                value={radius}
                className={styles.slider}
                onChange={value => this.handleOnChangeValue(value)}
              />
            </Col>
            <Col xs="auto">
              <InputGroup size="sm">
                <Input
                  type="number"
                  name="search-radius"
                  id="search-radius"
                  min="1"
                  max={MAX_RADIUS}
                  placeholder={MAX_RADIUS}
                  value={radius}
                  className={styles.input}
                  onChange={e => this.handleOnChangeValue(e.target.value)}
                />
                <InputGroupAddon addonType="append">
                  <InputGroupText>
                    km
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </Col>
          </Row>
          
        </FormGroup>
      </div>
    )
  }
}
