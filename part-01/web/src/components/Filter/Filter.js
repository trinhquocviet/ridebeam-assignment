import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Navbar,
  Row,
  Col,
  Button,
} from 'reactstrap';
import {
  pick,
  isEqual,
  isEmpty,
} from 'lodash';
import {
  MAX_RADIUS,
  DEFAULT_GEO_LOCATION,
  MAX_NUMBER_OF_SCOOTER,
} from 'common/constants/env';
import classNames from 'common/helpers/classnames';
import FormGroup from './components/FormGroup';
import SearchRadius from './components/SearchRadius';
import NumberOfScooters from './components/NumberOfScooters';
import Location from './components/Location';
import formActions from 'actions/form';
import scooterActions from 'actions/scooter';
import styles from './Filter.module.scss';

class Filter extends Component {
  static propTypes = {
    className:    PropTypes.string,
    location:     PropTypes.object,
    // 
    doSaveForm:   PropTypes.func,
    doGetScooter: PropTypes.func,
  }

  static defaultProps = {
    className:    '',
    location:     DEFAULT_GEO_LOCATION,
    // 
    doSaveForm:   () => {},
    doGetScooter: () => {},
  }

  state = {
    radius: 1,
    numberOfScooters: 100,
    location: DEFAULT_GEO_LOCATION,
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let newState = {};

    const [newLocation, oldLocation] = [nextProps.location, prevState.location];
    if (!isEqual(newLocation, oldLocation)) {
      newState.location = newLocation;
    }

    return (isEmpty(newState)) ? null : newState;
  }

  handleOnChange = (key, value) => {
    this.setState({ [key]: value }, () => this.saveFormData());
  }

  saveFormData = () => {
    const { doSaveForm } = this.props;
    doSaveForm(pick(this.state, ['radius', 'location']));
  }

  isAllowSubmit = () => {
    const { radius, numberOfScooters } = this.state;
    let allow = true;
    if (radius < 1 || radius > MAX_RADIUS) {
      allow = false;
    } else if (numberOfScooters < 1 || numberOfScooters > MAX_NUMBER_OF_SCOOTER) {
      allow = false;
    }

    return allow;
  }

  handleOnSubmit = () => {
    const { doGetScooter } = this.props;
    const { radius, numberOfScooters, location } = this.state;
    const query = {
      radius,
      numberOfScooters,
      ...location,
    }
    doGetScooter(query);
  }

  render = () => {
    const { className } = this.props;
    const {
      radius,
      numberOfScooters,
      location,
    } = this.state;

    return (
      <Navbar
        expand="md"
        className={classNames([styles.wrapper, className])}
      >
        <div className="flex-fill">
          <form onSubmit={e => { this.handleOnSubmit(); e.preventDefault(); }}>
            <Row className="h-100">
              <Col xs="12" md="auto" className="mb-3 mb-md-0">
                <SearchRadius
                  className={styles.searchRadius}
                  radius={radius}
                  onChange={value => this.handleOnChange('radius', value)}
                />
              </Col>
              <div className={styles.divider} />
              <Col xs="12" md="auto" className="mb-3 mb-md-0">
                <NumberOfScooters
                  className={styles.numberOfScooters}
                  numberOfScooters={numberOfScooters}
                  onChange={value => this.handleOnChange('numberOfScooters', value)}
                />
              </Col>
              <div className={styles.divider} />
              <Col xs="12" md="auto" className="mb-3 mb-md-0">
                <Location
                  className={styles.location}
                  location={location}
                  onChange={value => this.handleOnChange('location', value)}
                />
              </Col>
              <div className={styles.divider} />
              <Col xs="12" md="auto" className="mb-3 mb-md-0">
                <FormGroup label="&nbsp;" className={styles.submitBtn}>
                  <Button
                    type="submit"
                    size="sm"
                    color="primary"
                    disabled={!this.isAllowSubmit()}
                  >
                    Submit
                  </Button>
                </FormGroup>
              </Col>
            </Row>
          </form>
        </div>
      </Navbar>
    )
  }
}

const mapStateToProps = state => ({
  ...pick(state.form, [
    'location',
  ]),
});

const mapDispatchToProps = dispatch => ({
  doSaveForm: (formData) => dispatch(formActions.save(formData)),
  doGetScooter: (query) => dispatch(scooterActions.get(query)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
