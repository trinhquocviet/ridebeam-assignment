import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { GMAP_KEY, DEFAULT_GEO_LOCATION } from 'common/constants/env';
import { Map, Circle, Marker, GoogleApiWrapper } from 'google-maps-react';
import formActions from 'actions/form';

class ScooterMapContainer extends Component {
  static propTypes = {
    className:    PropTypes.string,
    radius:       PropTypes.number,
    location:     PropTypes.object,
    scooters:     PropTypes.array,
    // 
    doSaveForm:   PropTypes.func,
  }

  static defaultProps = {
    className:    '',
    radius:       0,
    location:     DEFAULT_GEO_LOCATION,
    scooters:     [],
    // 
    doSaveForm:   () => {},
  }

  handleOnMapClicked = (mapProps, map, clickEvent) => {
    const { doSaveForm } = this.props;
    doSaveForm({
      location: {
        lat: clickEvent.latLng.lat(),
        lng: clickEvent.latLng.lng(),
      }
    });
  }
    

  renderRadiusCircle = () => {
    const {
      radius,
      location,
    } = this.props;

    if (radius && !isEmpty(location)) {
      const transformedLocation = {
        lat: parseFloat(location.lat),
        lng: parseFloat(location.lng),
      }

      return (
        <Circle
          radius={radius * 1000}
          center={transformedLocation}
          strokeColor='transparent'
          strokeOpacity={0}
          strokeWeight={5}
          fillColor='#FF0000'
          fillOpacity={0.2}
        />
      );
    }

    return null;
  }

  

  renderScooterMarkers = () => {
    const { location } = this.props;
    return (
      <Marker
        name=""
        position={location}
        />
    );
  }
  
  renderCenterLocation = () => {
    const { scooters } = this.props;
    return scooters.map((s, idx) => (
      <Marker
        key={`scooter-marker-${idx}`}
        name={""}
        position={s.location}
        icon={{url: "/marker.png"}}/>
    ));
  }

  render = () => {
    const {
      location,
    } = this.props;

    return (
      <Map
        google={this.props.google}
        streetViewControl={false}
        rotateControl={false}
        mapTypeControl={false}
        initialCenter={DEFAULT_GEO_LOCATION}
        center={location}
        zoom={12}
        onClick={this.handleOnMapClicked}
      >
        { this.renderRadiusCircle() }
        { this.renderScooterMarkers() }
        { this.renderCenterLocation() }
      </Map>
    )
  }
}
const mapDispatchToProps = dispatch => ({
  doSaveForm: (formData) => dispatch(formActions.save(formData)),
});

export default connect(null, mapDispatchToProps)(
  GoogleApiWrapper({
    apiKey: (GMAP_KEY),
    language: 'en',
  })(ScooterMapContainer)
);
