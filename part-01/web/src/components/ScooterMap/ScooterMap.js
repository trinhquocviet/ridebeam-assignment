import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { pick } from 'lodash';
import { DEFAULT_GEO_LOCATION } from 'common/constants/env';
import ScooterMapContainer from './components/ScooterMapContainer';
import styles from './ScooterMap.module.scss';

class ScooterMap extends Component {
  static propTypes = {
    className:    PropTypes.string,
    radius:       PropTypes.number,
    location:     PropTypes.object,
    scooters:     PropTypes.array,
  }

  static defaultProps = {
    className:    '',
    radius:       0,
    location:     DEFAULT_GEO_LOCATION,
    scooters:     [],
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <ScooterMapContainer
          {...pick(this.props, ['radius', 'location', 'scooters'])}
        />
      </div>
    )
  }
}

export default ScooterMap;