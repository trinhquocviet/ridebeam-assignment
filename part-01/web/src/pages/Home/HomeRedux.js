import PropTypes from 'prop-types';
import { pick } from 'lodash';
import { DEFAULT_GEO_LOCATION } from 'common/constants/env';
import scooterActions from 'actions/scooter';

const propTypes = {
  scooters:           PropTypes.array,
  totalScooters:      PropTypes.number,

  isGettingScooters:  PropTypes.bool,
  getScootersError:   PropTypes.string,
  // 
  radius:             PropTypes.number,
  location:           PropTypes.object,
  // 
  doGetUsers:         PropTypes.func,
};

const defaultProps = {
  scooters:           [],
  totalScooters:      0,

  isGettingScooters:  false,
  getScootersError:   '',
  // 
  radius:             0,
  location:           DEFAULT_GEO_LOCATION,
  // 
  doGetScooters:      () => {},
};

const mapStateToProps = state => ({
  ...pick(state.scooter, [
    'scooters',
    'totalScooters',
    'isGettingScooters',
    'getScootersError',
  ]),
  ...pick(state.form, [
    'radius',
    'location',
  ]),
});

const mapDispatchToProps = dispatch => ({
  doGetScooters: () => dispatch(scooterActions.get()),
});

export {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
};