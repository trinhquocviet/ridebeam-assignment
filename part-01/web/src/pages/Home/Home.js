import React, { Component } from 'react';
import { connect } from 'react-redux';
import { pick } from 'lodash';
import {
  defaultProps,
  propTypes,
  mapStateToProps,
  mapDispatchToProps,
} from './HomeRedux';
import Layout from 'components/Layout';
import ScooterMap from 'components/ScooterMap';
import styles from './Home.module.scss';


class Home extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  render = () => {
    return (
      <Layout contentClass={styles.content}>
        <ScooterMap
          {...pick(this.props, ['radius', 'location', 'scooters'])}
        />
      </Layout>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);