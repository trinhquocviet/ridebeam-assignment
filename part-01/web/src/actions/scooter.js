import axios from 'common/helpers/axios';
import buildUrl from 'build-url';
import { API_SCOOTER } from 'common/constants/api';
import TYPES from 'common/constants/actionTypes';
import { actionCallAPI } from 'common/helpers/action';

const scooterActions = {
  get: (queryParams = {}) => (dispatch) => {
    const actionName = TYPES.SCOOTER.GET;
    const asyncAction = axios.GET(buildUrl(API_SCOOTER, { queryParams }));
  
    const actions = {
      success: async (response) => {
        if (response.status === 200) {
          const responseData = response.data || [];
          return {
            scooters: responseData,
            totalScooters: responseData.length,
          };
        }
        return null;
      },
      error: (response) => {
        return response.data.error || `${response.status}`;
      }
    };
  
    return dispatch(actionCallAPI(actionName, asyncAction, actions));
    
  }
}

export default scooterActions;