import TYPES from 'common/constants/actionTypes';

const formActions = {
  save: (formData) => (dispatch) => {
    return dispatch({
      type: TYPES.FORM.SAVE,
      payload: formData,
    });
  }
}

export default formActions;